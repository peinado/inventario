#!/usr/bin/env python3

inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # inventario[codigo] = [nombre, precio, cantidad]
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    for codigo in inventario:
        # print(codigo, inventario[codigo])
        valores = inventario[codigo]
        print(f'{codigo}: {valores["nombre"]}, precio: {valores["precio"]}, cantidad: {valores["cantidad"]}')
def consultar(codigo: str):
    valores = inventario[codigo]
    print(f'{codigo}: {valores["nombre"]}, precio: {valores["precio"]}, cantidad: {valores["cantidad"]}')
def agotados():
    for codigo in inventario:
        if inventario[codigo]["cantidad"] == 0:
            print(f'El articulo {codigo} se ha agotado')

def pide_articulo() -> (str, str, float, int):
    codigo = input("Codigo de articulo: ")
    nombre = input("Nombre: ")
    precio = input("Precio: ")
    cantidad = input("Cantidad: ")

    return codigo, nombre, precio, cantidad


def menu() -> int:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            insertar(pide_articulo())
        elif opcion == '2':
            listar()
        elif opcion == '3':
            consultar(codigo=input("Código de artículo: "))
        elif opcion == '4':
            agotados()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()
